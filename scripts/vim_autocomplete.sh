#!/bin/sh

mkdir -p $HOME/.vim/plugged/YouCompleteMe

cd $HOME/.vim/plugged/YouCompleteMe

git submodule update --init --recursive
./install.py

cd -
