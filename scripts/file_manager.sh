#!/bin/sh

sudo pacman -S --noconfirm ranger

ranger --copy-config=all

mkdir -p "$HOME/.config/ranger/plugins/ranger_devicons"

git clone https://github.com/alexanderjeurissen/ranger_devicons "$HOME/.config/ranger/plugins/ranger_devicons"

echo "default_linemode devicons" >> "$HOME/.config/ranger/rc.conf"
