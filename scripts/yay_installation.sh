#!/bin/sh

mkdir -p $HOME/yay
git clone https://aur.archlinux.org/yay.git $HOME/yay

cd $HOME/yay
yes | makepkg -si
cd -
