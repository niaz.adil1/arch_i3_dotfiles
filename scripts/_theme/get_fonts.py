import json
from urllib import request, parse
from pathlib import Path

themes = [
  ("AlienBlood.json",                     "alien_blood.json"),
  ("AtelierSulphurpool.json",             "atelier_sulphurpool.json"),
  ("Aurora.json",                         "aurora.json"),
  ("ayu_light.json",                      "ayu_light.json"),
  ("Batman.json",                         "batman.json"),
  ("Belafonte Day.json",                  "belafonte_day.json"),
  ("BlueDolphin.json",                    "blue_dolphin.json"),
  ("Blue Matrix.json",                    "blue_matrix.json"),
  ("BlulocoDark.json",                    "bluloco_dark.json"),
  ("BlulocoLight.json",                   "bluloco_light.json"),
  ("Breeze.json",                         "breeze.json"),
  ("Builtin Solarized Dark.json",         "builtin_solarized_dark.json"),
  ("Calamity.json",                       "calamity.json"),
  ("Chalk.json",                          "chalk.json"),
  ("Chester.json",                        "chester.json"),
  ("Cobalt Neon.json",                    "cobalt_neon.json"),
  ("Cyberdyne.json",                      "cyberdyne.json"),
  ("darkmatrix.json",                     "dark_matrix.json"),
  ("darkermatrix.json",                   "darker_matrix.json"),
  ("Darkside.json",                       "darkside.json"),
  ("Desert.json",                         "desert.json"),
  ("Django.json",                         "django.json"),
  ("DjangoRebornAgain.json",              "django_reborn_again.json"),
  ("DjangoSmooth.json",                   "django_smooth.json"),
  ("DoomOne.json",                        "doom_one.json"),
  ("Dracula+.json",                       "dracula.json"),
  ("Elemental.json",                      "elemental.json"),
  ("ENCOM.json",                          "encom.json"),
  ("Flat.json",                           "flat.json"),
  ("ForestBlue.json",                     "forest_blue.json"),
  ("idleToes.json",                       "idle_toes.json"),
  ("Lab Fox.json",                        "lab_fox.json"),
  ("lovelace.json",                       "lovelace.json"),
  ("MaterialDark.json",                   "material_dark.json"),
  ("matrix.json",                         "matrix.json"),
  ("Monokai Soda.json",                   "monokai_soda.json"),
  ("Neutron.json",                        "neutron.json"),
  ("nord.json",                           "nord.json"),
  ("OneHalfDark.json",                    "one_half_dark.json"),
  ("Rapture.json",                        "rapture.json"),
  ("rebecca.json",                        "rebecca.json"),
  ("Ryuuko.json",                         "ryuuko.json"),
  ("Seafoam Pastel.json",                 "seafoam_pastel.json"),
  ("Shaman.json",                         "shaman.json"),
  ("Slate.json",                          "slate.json"),
  ("Snazzy.json",                         "snazzy.json"),
  ("Solarized Dark Higher Contrast.json", "solarized_dark_higher_contrast.json"),
  ("Solarized Dark - Patched.json",       "solarized_dark_patched.json"),
  ("ToyChest.json",                       "toy_chest.json"),
  ("Violet Light.json",                   "violet_light.json"),
  ("Zenburn.json",                        "zenburn.json"),
]

home = str(Path.home())
output_directory = home + '/.config/theme_colors/themit/themes/'

for theme in themes:
  (src_name, dst_name) = theme
  param = parse.quote(src_name) 

  url_ = "https://raw.githubusercontent.com/mbadolato/iTerm2-Color-Schemes/master/windowsterminal/{}".format(param)

  with request.urlopen(url_) as url:
      data = json.loads(url.read().decode())

      formatted_theme = {
        "background":     data["background"],
        "foreground":     data["foreground"],
        "black_dark":     data["black"],
        "black_light":    data["brightBlack"],
        "red_dark":       data["red"],
        "red_light":      data["brightRed"],
        "green_dark":     data["green"],
        "green_light":    data["brightGreen"],
        "yellow_dark":    data["yellow"],
        "yellow_light":   data["brightYellow"],
        "blue_dark":      data["blue"],
        "blue_light":     data["brightBlue"],
        "magenta_dark":   data["purple"],
        "magenta_light":  data["brightPurple"],
        "cyan_dark":      data["cyan"],
        "cyan_light":     data["brightCyan"],
        "white_dark":     data["white"],
        "white_light":    data["brightWhite"],
      }
      json_object = json.dumps(formatted_theme, indent = 4)

      with open(output_directory + dst_name, "w") as outfile:
        outfile.write(json_object)
