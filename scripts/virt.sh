sudo pacman -S --noconfirm qemu virt-manager virt-viewer dnsmasq vde2 bridge-utils openbsd-netcat libguestfs dmidecode

sudo systemctl enable libvirtd.service
sudo systemctl start libvirtd.service

sudo sed -i 's/#unix_sock_group = "libvirt"/unix_sock_group = "libvirt"/g' /etc/libvirt/libvirtd.conf
sudo sed -i 's/#unix_sock_rw_perms = "0770"/unix_sock_rw_perms = "0770"/g' /etc/libvirt/libvirtd.conf

sudo usermod -a -G libvirt $USER
newgrp libvirt

sudo systemctl restart libvirtd.service

sudo modprobe -r kvm_intel
sudo modprobe kvm_intel nested=1

systool -m kvm_intel -v | grep nested
